// helper function
const enable_disable_btn_register_form = () => {
    const tnc = $("#inputCheckTnC").is(":checked")
    const resk = $('#inputCheckRisk').is(":checked")
    const btn = $("#btn-registrasi")

    if(tnc && resk) {
        btn.prop('disabled', false)
    }
    else {
        btn.prop('disabled', true)
    }
}
const get_string_by_language = () => {
    let data = {
        btn_register: 'Buat Akun',
        loading_btn_register: 'Mengirim data...',
        toggle_pwd_show: 'Tunjukkan Password',
        toggle_pwd_hide: 'Sembunyikan Password',
        tnc_URL: 'assets/modal/tnc.html',
        risk_URL: 'assets/modal/risk.html'
    }

    if (window.location.href.indexOf('/en/') >= 0) {
        data.btn_register = 'Create Account'
        data.loading_btn_register = 'Sending data...'
        data.toggle_pwd_show = 'Show Password'
        data.toggle_pwd_hide = 'Hide Password',
        data.tnc_URL = '../assets/modal/tnc-en.html',
        data.risk_URL = '../assets/modal/risk-en.html'
    }

    return data
}


// navbar effect after scroll
$(window).scroll(function() {
    if ($(document).scrollTop() > 100) {
        $('nav.navbar').addClass('scrolled');
    } else {
        $('nav.navbar').removeClass('scrolled');
    }
});


// toggle password
$( "#toggle-pwd" ).click(function() {
    const label = $('#toggle-pwd > small');
    const input = $('#inputPassword');

    if (label.text() === get_string_by_language().toggle_pwd_show) {
        label.text(get_string_by_language().toggle_pwd_hide)
        input.attr('type', 'text');
    } else {
        label.text(get_string_by_language().toggle_pwd_show)
        input.attr('type', 'password');
    }
});


//input phone number code list
const inputPhone = document.querySelector("#inputPhoneNumber");
window.intlTelInput(inputPhone, {
    preferredCountries: ['id'],
    // separateDialCode: true,
    // customPlaceholder: function(selectedCountryPlaceholder, selectedCountryData) {
    //     return "e.g. " + selectedCountryPlaceholder;
    // }
});


// enable or disable btn registrasi
$(".form-check-input").change(function() {
    enable_disable_btn_register_form();
});

// submit register form
$("#btn-registrasi").click(function() {
    $(`.form-group input`).removeClass('is-invalid')
    $(`.form-group .invalid-feedback`).removeClass('d-block').text('')
    $(this).text(get_string_by_language().loading_btn_register)

    const data = {
        hp: $("#inputPhoneNumber").val(),
        username: $("#inputUsername").val(),
        email: $("#inputEmail").val(),
        password: $("#inputPassword").val(),
        resiko: 'setuju',
        rsk: 'setuju',
        ref_username: 'salespage'
    }

    $.ajax({  
        url: 'https://indodax.com/api/v2/auth/register',  
        type: 'POST',
        headers: { 'is_salespage': '1' },
        dataType: 'json',  
        data: data,  
        success: function (data) {
            $(this).text(get_string_by_language().btn_register)
            if (data.success === true) {
                $('#form-response').append(
                    '<div class="alert alert-success" role="alert">' +
                        data.message +
                    '</div>'
                )
            }
            else {
                const props = Object.getOwnPropertyNames(data.message)

                props.map(item => {
                    $(`.${item} input`).addClass('is-invalid')
                    $(`.${item} .invalid-feedback`).addClass('d-block').text(resp.message[item])
                })
            }
        },  
        error: function (xhr, textStatus, errorThrown) {  
            $(this).text(get_string_by_language().btn_register)
            // console.log(xhr, textStatus, errorThrown);  
        }  
    }); 
    
});
//end submit register form


// checked the checkbox after click
$("#btn-submit-tnc").click(function(){
    $('#tncModal').modal('toggle');
    $("#inputCheckTnC").prop("checked", true);
    enable_disable_btn_register_form();
});
$("#btn-submit-risk").click(function(){
    $('#tradingRiskModal').modal('toggle');
    $("#inputCheckRisk").prop("checked", true);
    enable_disable_btn_register_form();
});
//end checked the checkbox after click


// enable modal btn when finish read modal
$('#tncModal .modal-body').scroll(function () {
    const theContent = $('#tncModal .the-content').height()

    if ($(this).height() + $(this).scrollTop() > theContent) {
        $('#btn-submit-tnc').prop('disabled', false)
    } 
});
$('#tradingRiskModal .modal-body').scroll(function () {
    const theContent = $('#tradingRiskModal .the-content').height()

    if ($(this).height() + $(this).scrollTop() > theContent) {
        $('#btn-submit-risk').prop('disabled', false)
    } 
});
//end  enable modal btn when finish read modal


$( document ).ready(function() {
    $.get(get_string_by_language().tnc_URL, function(data){
        $('#tnc-content').append(data)
    });
    $.get(get_string_by_language().risk_URL, function(data){
        $('#risk-content').append(data)
    });
});